package crawlers;

import org.apache.log4j.BasicConfigurator;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.FilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

public class GithubCrawler implements PageProcessor {
    private Site site = Site.me()
            .setRetryTimes(3)
            .setSleepTime(1000);

    public void process(Page page) {
        page.putField("singleRepo", page.getHtml().$(".d-inline-block").toString());
        page.addTargetRequests(page.getHtml().links().regex("\\/code4craft([?a-zA-Z=1-9&]+)").all());
    }

    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();

        Spider spider = Spider.create(new GithubCrawler());

        spider.thread(3)
                .addUrl("https://github.com/code4craft?tab=repositories")
                .addPipeline(new FilePipeline("/tmp"))
                .run();
    }
}
