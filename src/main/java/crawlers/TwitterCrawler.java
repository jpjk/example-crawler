package crawlers;

import org.apache.log4j.BasicConfigurator;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.FilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

public class TwitterCrawler implements PageProcessor {

    private Site site = Site.me()
            .setRetryTimes(3)
            .setSleepTime(1000);

    public void process(Page page) {
        page.putField("user", page.getHtml().$("a.ProfileHeaderCard-nameLink").toString());

        if (page.getResultItems().get("user") == null) {
            page.setSkip(true);
        }
    }

    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();

        String[] urls = {
                "https://twitter.com/janrostowski",
                "https://twitter.com/coimbragroup",
                "https://twitter.com/uniturku",
                "https://twitter.com/unibarcelona",
                "https://twitter.com/thehistoryguy",
                "http://httpbin.org/status/500",
                "https://twitter.com/reuters",
                "https://twitter.com/nba",
                "https://twitter.com/princeton",
                "https://twitter.com/unesco",
                "https://twitter.com/ashaphillips"
        };

        Spider spider = Spider.create(new TwitterCrawler());

        for (String url : urls) {
            spider.addUrl(url);
        }

        spider.thread(3)
              .addPipeline(new FilePipeline("/tmp"))
              .run();
    }
}
