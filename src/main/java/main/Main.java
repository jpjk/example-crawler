package main;

import crawlers.GithubCrawler;
import crawlers.TwitterCrawler;

public class Main {
    public static void main(String[] args) {
        GithubCrawler.main(args);
        TwitterCrawler.main(args);
    }
}
